package br.com.neomode.httploader.http;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.AsyncTaskLoader;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

import br.com.neomode.httploader.R;
import br.com.neomode.httploader.connectivity.ConnectivityCheck;
import br.com.neomode.httploader.databinding.ErrorViewBinding;
import br.com.neomode.httploader.databinding.LoadingViewBinding;
import br.com.neomode.httploader.utils.NeomodeTextUtils;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by John on 14/09/16.
 * Please add lines to this archive with all the love in the universe!
 * If you have problems with the code written right over here, please mail me: joao@neomode.com.br
 */

public class HTTPLoader extends AsyncTaskLoader<HTTPResponse> {

    private static final int RESPONSE_CODE_EXCEPTION = 701;
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    public static final MediaType URL_ENCODED = MediaType.parse("application/x-www-form-urlencoded; charset=utf-8");

    // Objects that will be used to CREATE an Http Request
    private @NonNull
    Context mRequestContext;
    private @Nullable
    ViewGroup mRequestView;
    private @NonNull
    HTTPVerb mRequestVerb;
    private @NonNull
    MediaType mRequestMediaType;
    private @NonNull
    String mRequestUrl;
    private @Nullable
    HashMap<String, String> mRequestParameters;
    private @Nullable
    String mRequestParametersString;
    private @Nullable
    HashMap<String, String> mRequestHeaders;
    private @Nullable
    String mLoadingMessage;
    // Object that will be delivered as result
    private HTTPResponse mRequestResponse;
    // View objects to handle the loading & the error
    private ErrorViewBinding mErrorBinding;
    private LoadingViewBinding mLoadingBinding;

    private static @Nullable OnUnauthorizedRequestListener mUnauthorizedListener;
    private static @Nullable IResponseErrorParser mErrorParser;

    /**
     * Initialize all the variables that will be necessary at {@link HTTPLoader#loadInBackground()} when we make the Http Request
     * @param context a {@link Context} to be used on the parent {@link AsyncTaskLoader}
     * @param requestView a {@link ViewGroup} that will receive the UI feedback of the loadings
     * @param requestVerb a {@link HTTPVerb} that indicates the type of the request
     * @param requestMediaType a {@link MediaType} to make the request
     * @param requestUrl a valid http link that will be called by the http request
     * @param requestParams if not null, the parameters to be passed on the http request
     * @param requestHeaders if not null, the headers to be used on the http request
     */
    public HTTPLoader(@NonNull Context context, @Nullable ViewGroup requestView, @NonNull HTTPVerb requestVerb, @NonNull MediaType requestMediaType, @NonNull String requestUrl,
                      @Nullable HashMap<String, String> requestParams, @Nullable HashMap<String, String> requestHeaders) {
        super(context);
        mRequestContext = context;
        mRequestView = requestView;
        mRequestVerb = requestVerb;
        mRequestMediaType = requestMediaType;
        mRequestUrl = requestUrl;
        mRequestParameters = requestParams;
        mRequestHeaders = requestHeaders;
        mLoadingMessage = null;
    }

    /**
     * Initialize all the variables that will be necessary at {@link HTTPLoader#loadInBackground()} when we make the Http Request
     * @param context a {@link Context} to be used on the parent {@link AsyncTaskLoader}
     * @param requestView a {@link ViewGroup} that will receive the UI feedbacks of the loadings
     * @param requestVerb a {@link HTTPVerb} that indicates the type of the request
     * @param requestMediaType a {@link MediaType} to make the request
     * @param requestUrl a valid http link that will be called by the http request
     * @param requestParams if not null, the parameters to be passed on the http request formatted from a {@link JSONObject#toString()}
     * @param requestHeaders if not null, the headers to be used on the http request
     */
    public HTTPLoader(@NonNull Context context, @Nullable ViewGroup requestView, @NonNull HTTPVerb requestVerb, @NonNull MediaType requestMediaType, @NonNull String requestUrl,
                      @Nullable String requestParams, @Nullable HashMap<String, String> requestHeaders) {
        super(context);
        mRequestContext = context;
        mRequestView = requestView;
        mRequestVerb = requestVerb;
        mRequestMediaType = requestMediaType;
        mRequestUrl = requestUrl;
        mRequestParametersString = requestParams;
        mRequestHeaders = requestHeaders;
        mLoadingMessage = null;
    }

    /**
     * Initialize all the variables that will be necessary at {@link HTTPLoader#loadInBackground()} when we make the Http Request
     * @param context a {@link Context} to be used on the parent {@link AsyncTaskLoader}
     * @param requestView a {@link ViewGroup} that will receive the UI feedbacks of the loadings
     * @param requestVerb a {@link HTTPVerb} that indicates the type of the request
     * @param requestMediaType a {@link MediaType} to make the request
     * @param requestUrl a valid http link that will be called by the http request
     * @param requestParams if not null, the parameters to be passed on the http request formatted from a {@link JSONObject#toString()}
     * @param requestHeaders if not null, the headers to be used on the http request
     * @param loadingMessage if not null, the string that will be shown with the loading
     */
    public HTTPLoader(@NonNull Context context, @Nullable ViewGroup requestView, @NonNull HTTPVerb requestVerb, @NonNull MediaType requestMediaType, @NonNull String requestUrl,
                      @Nullable String requestParams, @Nullable HashMap<String, String> requestHeaders, @Nullable String loadingMessage) {
        super(context);
        mRequestContext = context;
        mRequestView = requestView;
        mRequestVerb = requestVerb;
        mRequestMediaType = requestMediaType;
        mRequestUrl = requestUrl;
        mRequestParametersString = requestParams;
        mRequestHeaders = requestHeaders;
        mLoadingMessage = loadingMessage;
    }

    /**
     * Initialize all the variables that will be necessary at {@link HTTPLoader#loadInBackground()} when we make the Http Request
     * @param context a {@link Context} to be used on the parent {@link AsyncTaskLoader}
     * @param requestView a {@link ViewGroup} that will receive the UI feedbacks of the loadings
     * @param requestVerb a {@link HTTPVerb} that indicates the type of the request
     * @param requestMediaType a {@link MediaType} to make the request
     * @param requestUrl a valid http link that will be called by the http request
     * @param requestParams if not null, the parameters to be passed on the http request
     * @param requestHeaders if not null, the headers to be used on the http request
     * @param loadingMessage if not null, the string that will be shown with the loading
     */
    public HTTPLoader(@NonNull Context context, @Nullable ViewGroup requestView, @NonNull HTTPVerb requestVerb, @NonNull MediaType requestMediaType, @NonNull String requestUrl,
                      @Nullable HashMap<String, String> requestParams, @Nullable HashMap<String, String> requestHeaders, @Nullable String loadingMessage) {
        super(context);
        mRequestContext = context;
        mRequestView = requestView;
        mRequestVerb = requestVerb;
        mRequestMediaType = requestMediaType;
        mRequestUrl = requestUrl;
        mRequestParameters = requestParams;
        mRequestHeaders = requestHeaders;
        mLoadingMessage = loadingMessage;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        if (mRequestResponse != null) {
            return;
        }
        // Show the loading to the user
        showLoading();
        // TODO: We really need to cache the result?
        // If we are starting the Loader and already have the result, just deliver it
        // if (mRequestResponse != null && mRequestResponse.isResponseValid()) {
        //     deliverResult(mRequestResponse);
        // } else {
        //     forceLoad();
        // }
        forceLoad();
    }

    @Override
    public HTTPResponse loadInBackground() {
        // Create all the initials items that we need to do the request
        OkHttpClient requestClient = new OkHttpClient();
        Request.Builder requestBuilder = new Request.Builder();
        // Add all defaults headers
        HashMap<String, String> defaultHeaders = HTTPLoaderHelper.getDefaultHeaders(mRequestContext);
        for(String key: defaultHeaders.keySet()) {
            requestBuilder.addHeader(key, defaultHeaders.get(key));
        }
        // Add the header to the request
        if (mRequestHeaders != null) {
            for (String key : mRequestHeaders.keySet()) {
                requestBuilder.addHeader(key, mRequestHeaders.get(key));
            }
        }
        // Set the method and the request body
        if (mRequestVerb != HTTPVerb.GET) {
            if (mRequestParameters != null) {
                requestBuilder.method(mRequestVerb.name(), createBodyWithMediaType());
            } else if (!TextUtils.isEmpty(mRequestParametersString)) {
                requestBuilder.method(mRequestVerb.name(), createBodyWithMediaType());
            } else {
                requestBuilder.method(mRequestVerb.name(), createBodyWithMediaType());
            }
        }
        // Set the url
        requestBuilder.url(mRequestUrl);
        // Now do the call, parse the result and return to end this method's work
        HTTPResponse requestResponse;
        try {
            Response apiCallResponse = requestClient.newCall(requestBuilder.build()).execute();
            requestResponse = new HTTPResponse(apiCallResponse.code(), apiCallResponse.isSuccessful(), getResponseString(apiCallResponse));
            // Check if the returned JSON is a valid one, otherwise we can't assume that this request is success
            if (apiCallResponse.isSuccessful()) {
                if (!NeomodeTextUtils.isJSONValid(requestResponse.getResponseMessage())) {
                    requestResponse = new HTTPResponse(RESPONSE_CODE_EXCEPTION, false, "");
                }
            }
        } catch (Exception e) {
            // If exceptions occur when parsing the result, we build a response that has an error
            requestResponse = new HTTPResponse(RESPONSE_CODE_EXCEPTION, false, e.getMessage());
        }
        return requestResponse;
    }

    @Override
    public void deliverResult(HTTPResponse result) {
        if (isReset()) {
            return;
        }
        mRequestResponse = result;
        // After doing all this steps, we need to check if the HTTPResponse is successful,
        // if not, we need to show the error view and register to automatic retry
        if (mRequestResponse != null && !mRequestResponse.isSuccess()) {
            if (mRequestResponse.getStatusCode() == 401) {
                if (isReset()) {
                    return;
                }
                // If we receive a 401 from the API, we need to check if we have any defined interface
                // That will handle the token refresh
                if (mUnauthorizedListener != null) {
                    mUnauthorizedListener.onUnauthorizedRequest(new OnUnauthorizedResultListener() {
                        @Override
                        public void onUnauthorizedResultSuccess() {
                            hideLoading();
                            forceLoad();
                        }

                        @Override
                        public void onUnauthorizedResultFailure() {
                            hideLoading();
                            showTryAgain();
                        }
                    });
                    return;
                }
            }
            showTryAgain();
        }
        hideLoading();
        if (isStarted() && mRequestResponse.getStatusCode() != 401) {
            if (isReset()) {
                return;
            }
            super.deliverResult(result);
        }
    }

    @Override
    protected void onStopLoading() {
        // Ensure that the loading is cancelled
        cancelLoad();
        // Clean the resources
        releaseLoaderResources();
    }

    @Override
    public void onCanceled(HTTPResponse data) {
        super.onCanceled(data);
        // Clean the resources
        releaseLoaderResources();
        // Hide the loading view to ensure that no traces of this remain in the UI
        hideLoading();
    }

    @Override
    protected void onReset() {
        // Stop the work
        super.onReset();
        onStopLoading();
        // Clean the resources
        releaseLoaderResources();
        // Hide the loading view to ensure that no traces of this remain in the UI
        hideLoading();
    }

    /**
     * Responsible to managing the {@link LoadingViewBinding} into the views that are calling
     * this Loader. We do all the automatic job, so you don't need to add views of loading and/or error.
     */
    private void showLoading() {
        if (mRequestView != null) {
            // Hide any error presented on the screen
            hideTryAgain();
            // Check if the layout already has a loading view, if not we add one
            View rootViewLoading = mRequestView.findViewById(R.id.viewLoading);
            if (rootViewLoading == null) {
                // Create the loading view binding
                mLoadingBinding = DataBindingUtil.inflate(LayoutInflater.from(mRequestContext), R.layout.loading_view, mRequestView, false);
                // And then add it to the root view
                mRequestView.post(new Runnable() {
                    @Override
                    public void run() {
                        mRequestView.addView(mLoadingBinding.getRoot());
                        ViewGroup.LayoutParams params = mLoadingBinding.getRoot().getLayoutParams();
                        params.height = mRequestView.getHeight();
                        mLoadingBinding.getRoot().setLayoutParams(params);
                    }
                });
            } else {
                // Otherwise we just need to obtain the binding using the rootView
                mLoadingBinding = DataBindingUtil.bind(rootViewLoading);
            }
            // Now we just show the loading view to make a UI awareness to the user
            mLoadingBinding.viewLoading.setVisibility(View.VISIBLE);
            mLoadingBinding.textLoading.setText(mLoadingMessage);
            mLoadingBinding.textLoading.setVisibility(!TextUtils.isEmpty(mLoadingMessage) ? View.VISIBLE : View.GONE);
        }
    }

    /**
     * Hide the loading that might be presented on the UI
     */
    private void hideLoading() {
        if (mLoadingBinding != null) {
            mLoadingBinding.viewLoading.setVisibility(View.GONE);
        }
    }

    /**
     * Responsible to managing the {@link ErrorViewBinding} into the views that are calling
     * this Loader. We do all the automatic job, so you don't need to add views of loading and/or error.
     */
    private void showTryAgain() {
        if (mRequestView != null && mRequestResponse != null) {
            // Check if the layout already has a error view, if not we need to add one
            View rootViewError = mRequestView.findViewById(R.id.viewError);
            if (rootViewError == null) {
                // Create the error view binding
                mErrorBinding = DataBindingUtil.inflate(LayoutInflater.from(mRequestContext), R.layout.error_view, mRequestView, false);
                // And then add it to the root view
                mRequestView.post(new Runnable() {
                    @Override
                    public void run() {
                        mRequestView.addView(mErrorBinding.getRoot());
                        ViewGroup.LayoutParams params = mErrorBinding.getRoot().getLayoutParams();
                        params.height = mRequestView.getHeight();
                        mErrorBinding.getRoot().setLayoutParams(params);
                    }
                });
            } else {
                // Otherwise we just need to obtain the binding using the rootView that was found by findViewById
                mErrorBinding = DataBindingUtil.bind(rootViewError);
            }
            // Now we just show the loading view to make a UI awareness to the user
            mErrorBinding.viewError.setVisibility(View.VISIBLE);
            mErrorBinding.textError.setText(mRequestResponse.getStatusCode() == RESPONSE_CODE_EXCEPTION ? mRequestContext.getString(R.string.s_generic_api_error) : mRequestResponse.getResponseMessage());
            mErrorBinding.buttonTryAgain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    retryLoader();
                }
            });
        }
        // Register a listener to do a automatic retry when a call is not successful.
        // This is great, because sometimes the user wont click the retry button, but we do it automagic
        if (!ConnectivityCheck.getInstance().isHasConnectivity()) {
            ConnectivityCheck.getInstance().registerObserver(new Observer() {
                @Override
                public void update(Observable observable, Object o) {
                    if (ConnectivityCheck.getInstance().isHasConnectivity()) {
                        ConnectivityCheck.getInstance().unregisterObserver(this);
                        retryLoader();
                    }
                }
            });
        }
    }

    /**
     * Hide the error view that might be presented on the UI
     */
    private void hideTryAgain() {
        if (mErrorBinding != null)
            mErrorBinding.viewError.setVisibility(View.GONE);
    }

    /**
     * Extract the result from the HTTP Request.
     * @param response {@link Response} from the API
     * @return a {@link String} containing the result
     */
    private String getResponseString(@NonNull Response response) {
        try {
            String fullResponse = response.body().string();
            if (!response.isSuccessful() && mErrorParser != null) {
                return mErrorParser.parseResponseError(fullResponse);
            }
            return fullResponse;
        } catch (IOException e) {
            return e.getLocalizedMessage();
        }
    }

    /**
     * Create a {@link RequestBody} using the {@link HTTPLoader#mRequestParameters} or {@link HTTPLoader#mRequestParametersString}
     * and {@link HTTPLoader#mRequestMediaType}.
     * @return a {@link RequestBody} if any body available or null
     */
    private @Nullable RequestBody createBodyWithMediaType() {
        if (mRequestMediaType == URL_ENCODED) {
            if (mRequestParameters != null) {
                FormBody.Builder builder = new FormBody.Builder();
                for (String key : mRequestParameters.keySet()) {
                    builder.addEncoded(key, mRequestParameters.get(key));
                }
                return builder.build();
            }
            return null;
        }
        if (mRequestParameters != null) {
            return RequestBody.create(JSON, new JSONObject(mRequestParameters).toString());
        } else if (mRequestParametersString != null) {
            return RequestBody.create(JSON, mRequestParametersString);
        }
        return null;
    }

    /**
     * Restart the loader and do the HTTP request again
     */
    private void retryLoader() {
        hideTryAgain();
        showLoading();
        forceLoad();
    }

    /**
     * Release all the resources used on this Loader
     */
    private void releaseLoaderResources() {
        if (mRequestResponse != null) {
            mRequestResponse = null;
        }
    }

    // Static methods
    public static void setUnauthorizedListener(@Nullable OnUnauthorizedRequestListener listener) {
        mUnauthorizedListener = listener;
    }

    public interface OnUnauthorizedRequestListener {
        void onUnauthorizedRequest(@NonNull OnUnauthorizedResultListener resultListener);
    }

    public interface OnUnauthorizedResultListener {
        void onUnauthorizedResultSuccess();
        void onUnauthorizedResultFailure();
    }

    // Methods to enable the error handling
    public static void setErrorResponseParser(@NonNull IResponseErrorParser errorParser) {
        mErrorParser = errorParser;
    }

    public interface IResponseErrorParser {
        @NonNull String parseResponseError(@NonNull String fullResponse);
    }

}
