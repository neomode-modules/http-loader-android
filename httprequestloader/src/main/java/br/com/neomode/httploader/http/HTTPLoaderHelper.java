package br.com.neomode.httploader.http;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import java.util.HashMap;

/**
 * Created by John on 4/17/17.
 * Please add lines to this archive with all the love in the universe!
 * If you have problems with the code written right over here, please mail me: joao@neomode.com.br
 */
public abstract class HTTPLoaderHelper {

    private static final String LOADER_PREFERENCES = "loader_pref";
    private static final String KEY_FOR_DEFAULT_KEYS = "x_keys";
    private static final String KEY_FOR_DEFAULT_VALUES = "x_values";

    /**
     * Call this method whenever you want to set default headers. Default headers will be add to all requests made by this library.
     * @param context {@link Context} used to obtain the {@link android.content.SharedPreferences.Editor}
     * @param defaultHeaders {@link HashMap} containing the default headers
     */
    public static void addDefaultHeaders(@NonNull Context context, @NonNull HashMap<String, String> defaultHeaders) {
        // To save the default headers we make a simple workout. We need to save two Set<String>. One with the keys and other with the values
        String sKeys = join(defaultHeaders.keySet().toArray(new String[defaultHeaders.keySet().size()]));
        String sValues = join(defaultHeaders.values().toArray(new String[defaultHeaders.values().size()]));
        // And then save it to the preferences
        getDefaultPreferencesEditor(context).putString(KEY_FOR_DEFAULT_KEYS, sKeys)
                .putString(KEY_FOR_DEFAULT_VALUES, sValues).commit();
    }

    /**
     * Return all default headers used on all HTTP requests
     * @param context context {@link Context} used to obtain the {@link android.content.SharedPreferences}
     * @return {@link HashMap} containing headers keys and values
     */
    public static HashMap<String, String> getDefaultHeaders(@NonNull Context context) {
        SharedPreferences sharedPreferences = getDefaultPreferences(context);
        String keys = sharedPreferences.getString(KEY_FOR_DEFAULT_KEYS, "");
        String values = sharedPreferences.getString(KEY_FOR_DEFAULT_VALUES, "");
        // Create a HashMap to return the default values
        HashMap<String, String> defaultHeaders = new HashMap<>();
        if (!TextUtils.isEmpty(keys)) {
            String[] aKeys = keys.split(";");
            String[] aValues = values.split(";");
            for (int i = 0; i < aKeys.length; i++) {
                defaultHeaders.put(aKeys[i], aValues[i]);
            }
        }
        return defaultHeaders;
    }

    /**
     * Rest the default headers to the empty state
     * @param context {@link Context} used to obtain the {@link android.content.SharedPreferences.Editor}
     */
    public static void clearDefaultHeaders(@NonNull Context context) {
        getDefaultPreferencesEditor(context).putString(KEY_FOR_DEFAULT_KEYS, "")
                .putString(KEY_FOR_DEFAULT_VALUES, "").commit();
    }

    private static SharedPreferences getDefaultPreferences(@NonNull Context context) {
        return context.getSharedPreferences(LOADER_PREFERENCES, Context.MODE_PRIVATE);
    }

    private static SharedPreferences.Editor getDefaultPreferencesEditor(@NonNull Context context) {
        return getDefaultPreferences(context).edit();
    }

    private static String join(@NonNull String[] set) {
        String result = "";
        for (String s: set) {
            if (!TextUtils.isEmpty(result)) result += ";";
            result += s;
        }
        return result;
    }

}
