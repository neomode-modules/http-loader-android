package br.com.neomode.httploader.http;

/**
 * Created by John on 14/09/16.
 * Please add lines to this archive with all the love in the universe!
 * If you have problems with the code written right over here, please mail me: joao@neomode.com.br
 */

public enum HTTPVerb {
    GET, POST, PUT, PATCH, DELETE;
}
