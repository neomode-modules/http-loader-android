package br.com.neomode.httploader.http;

import android.support.annotation.NonNull;

/**
 * Created by John on 14/09/16.
 * Please add lines to this archive with all the love in the universe!
 * If you have problems with the code written right over here, please mail me: joao@neomode.com.br
 */

public class HTTPResponse {

    private int statusCode;
    private boolean isSuccess;
    private String responseMessage;

    public HTTPResponse(int statusCode, boolean isSuccess, @NonNull String responseMessage) {
        this.statusCode = statusCode;
        this.isSuccess = isSuccess;
        this.responseMessage = responseMessage;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

}
