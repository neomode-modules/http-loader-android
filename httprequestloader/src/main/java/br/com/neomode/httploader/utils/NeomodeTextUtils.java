package br.com.neomode.httploader.utils;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by John on 20/09/16.
 * Please add lines to this archive with all the love in the universe!
 * If you have problems with the code written right over here, please mail me: joao@neomode.com.br
 */

public abstract class NeomodeTextUtils {

    public static boolean isJSONValid(@NonNull String test) {
        // If the string is empty, we consider its valid
        if (TextUtils.isEmpty(test))
            return true;

        try {
            new JSONObject(test);
        } catch (JSONException ex) {
            try {
                new JSONArray(test);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }

}
