package br.com.neomode.httploader.http;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashMap;

import okhttp3.MediaType;

/**
 * Created by John on 4/17/17.
 * Please add lines to this archive with all the love in the universe!
 * If you have problems with the code written right over here, please mail me: joao@neomode.com.br
 */
public abstract class HTTPController {

    /**
     * Create, start and delivery a HTTP call attaching it to any context, forcing this to be bind with {@link android.support.v4.app.Fragment} or
     * {@link android.app.Activity} lifecycle.
     * @param requestLoaderId an arbitrary unique identifier for the loader. <b>Must be unique by request</b>.
     * @param context a {@link Context} to be used as resource loader from the {@link HTTPLoader}.
     * @param loaderManager a {@link LoaderManager} from a {@link AppCompatActivity#getSupportFragmentManager()} or {@link Fragment#getFragmentManager()} to bind the loader.
     * @param viewLoading a {@link ViewGroup} to receive the loading and/or the error view from the HTTP Request.
     * @param requestUrl a {@link String} with the formatted url that must be called.
     * @param requestVerb a {@link HTTPVerb} to indicate the type of the HTTP Request.
     * @param params a {@link HashMap} with the params to add to the HTTP Request.
     * @param headers a {@link HashMap} with additional headers to add to the HTTP Request. <b>the {@link HTTPLoaderHelper#getDefaultHeaders(Context)} doesn't need to be used here as parameter.</b>
     * @param callback a {@link OnControllerResult} to be called when the request is finish.
     * @param <T> the return type to use when parsing the result to delivery by {@link OnControllerResult#onResultSuccess(T)}.
     */
    public static <T> void doHTTPRequest(final int requestLoaderId, @NonNull final Context context, @NonNull final LoaderManager loaderManager, @Nullable final ViewGroup viewLoading,
            @NonNull final String requestUrl, @NonNull final HTTPVerb requestVerb, @Nullable final HashMap<String, String> params,
            @Nullable final HashMap<String, String> headers, @Nullable final OnControllerResult<T> callback) {
        doHTTPRequest(requestLoaderId, context, loaderManager, viewLoading, requestUrl, requestVerb, HTTPLoader.JSON, params, headers, callback);
    }

    /**
     * Create, start and delivery a HTTP call attaching it to any context, forcing this to be bind with {@link android.support.v4.app.Fragment} or
     * {@link android.app.Activity} lifecycle.
     * @param requestLoaderId an arbitrary unique identifier for the loader. <b>Must be unique by request</b>.
     * @param context a {@link Context} to be used as resource loader from the {@link HTTPLoader}.
     * @param loaderManager a {@link LoaderManager} from a {@link AppCompatActivity#getSupportFragmentManager()} or {@link Fragment#getFragmentManager()} to bind the loader.
     * @param viewLoading a {@link ViewGroup} to receive the loading and/or the error view from the HTTP Request.
     * @param requestUrl a {@link String} with the formatted url that must be called.
     * @param requestVerb a {@link HTTPVerb} to indicate the type of the HTTP Request.
     * @param mediaType {@link HTTPLoader#JSON} or {@link HTTPLoader#URL_ENCODED}
     * @param params a {@link HashMap} with the params to add to the HTTP Request.
     * @param headers a {@link HashMap} with additional headers to add to the HTTP Request. <b>the {@link HTTPLoaderHelper#getDefaultHeaders(Context)} doesn't need to be used here as parameter.</b>
     * @param callback a {@link OnControllerResult} to be called when the request is finish.
     * @param <T> the return type to use when parsing the result to delivery by {@link OnControllerResult#onResultSuccess(T)}.
     */
    public static <T> void doHTTPRequest(final int requestLoaderId, @NonNull final Context context, @NonNull final LoaderManager loaderManager, @Nullable final ViewGroup viewLoading,
                                         @NonNull final String requestUrl, @NonNull final HTTPVerb requestVerb, @NonNull final MediaType mediaType, @Nullable final HashMap<String, String> params,
                                         @Nullable final HashMap<String, String> headers, @Nullable final OnControllerResult<T> callback) {
        // TODO: using restartLoader is the correct/better approach? Need to do a more deep research about it.
        doHTTPRequest(requestLoaderId, context, loaderManager, viewLoading, requestUrl, requestVerb, mediaType, params, null, headers, null, callback);
    }

    /**
     * Create, start and delivery a HTTP call attaching it to any context, forcing this to be bind with {@link android.support.v4.app.Fragment} or
     * {@link android.app.Activity} lifecycle.
     * @param requestLoaderId an arbitrary unique identifier for the loader. <b>Must be unique by request</b>.
     * @param context a {@link Context} to be used as resource loader from the {@link HTTPLoader}.
     * @param loaderManager a {@link LoaderManager} from a {@link AppCompatActivity#getSupportFragmentManager()} or {@link Fragment#getFragmentManager()} to bind the loader.
     * @param viewLoading a {@link ViewGroup} to receive the loading and/or the error view from the HTTP Request.
     * @param requestUrl a {@link String} with the formatted url that must be called.
     * @param requestVerb a {@link HTTPVerb} to indicate the type of the HTTP Request.
     * @param mediaType {@link HTTPLoader#JSON} or {@link HTTPLoader#URL_ENCODED}
     * @param params a {@link HashMap} with the params to add to the HTTP Request.
     * @param headers a {@link HashMap} with additional headers to add to the HTTP Request. <b>the {@link HTTPLoaderHelper#getDefaultHeaders(Context)} doesn't need to be used here as parameter.</b>
     * @param callback a {@link OnControllerResult} to be called when the request is finish.
     * @param <T> the return type to use when parsing the result to delivery by {@link OnControllerResult#onResultSuccess(T)}.
     */
    public static <T> void doHTTPRequest(final int requestLoaderId, @NonNull final Context context, @NonNull final LoaderManager loaderManager, @Nullable final ViewGroup viewLoading,
                                         @NonNull final String requestUrl, @NonNull final HTTPVerb requestVerb, @NonNull final MediaType mediaType, @Nullable final HashMap<String, String> paramsMap, @Nullable final String params,
                                         @Nullable final HashMap<String, String> headers, @Nullable final OnControllerResult<T> callback) {
        doHTTPRequest(requestLoaderId, context, loaderManager, viewLoading, requestUrl, requestVerb, mediaType, paramsMap, params, headers, null, callback);
    }

    /**
     * Create, start and delivery a HTTP call attaching it to any context, forcing this to be bind with {@link android.support.v4.app.Fragment} or
     * {@link android.app.Activity} lifecycle.
     * @param requestLoaderId an arbitrary unique identifier for the loader. <b>Must be unique by request</b>.
     * @param context a {@link Context} to be used as resource loader from the {@link HTTPLoader}.
     * @param loaderManager a {@link LoaderManager} from a {@link AppCompatActivity#getSupportFragmentManager()} or {@link Fragment#getFragmentManager()} to bind the loader.
     * @param viewLoading a {@link ViewGroup} to receive the loading and/or the error view from the HTTP Request.
     * @param requestUrl a {@link String} with the formatted url that must be called.
     * @param requestVerb a {@link HTTPVerb} to indicate the type of the HTTP Request.
     * @param mediaType {@link HTTPLoader#JSON} or {@link HTTPLoader#URL_ENCODED}
     * @param params a {@link HashMap} with the params to add to the HTTP Request.
     * @param headers a {@link HashMap} with additional headers to add to the HTTP Request. <b>the {@link HTTPLoaderHelper#getDefaultHeaders(Context)} doesn't need to be used here as parameter.</b>
     * @param loadingMessage if not null, the string that will be shown with the loading
     * @param callback a {@link OnControllerResult} to be called when the request is finish.
     * @param <T> the return type to use when parsing the result to delivery by {@link OnControllerResult#onResultSuccess(T)}.
     */
    public static <T> void doHTTPRequest(final int requestLoaderId, @NonNull final Context context, @NonNull final LoaderManager loaderManager, @Nullable final ViewGroup viewLoading,
                                         @NonNull final String requestUrl, @NonNull final HTTPVerb requestVerb, @NonNull final MediaType mediaType, @Nullable final HashMap<String, String> paramsMap, @Nullable final String params,
                                         @Nullable final HashMap<String, String> headers, @Nullable final String loadingMessage, @Nullable final OnControllerResult<T> callback) {
        loaderManager.restartLoader(requestLoaderId, null, new LoaderManager.LoaderCallbacks<HTTPResponse>() {
            @Override
            public Loader<HTTPResponse> onCreateLoader(int id, Bundle args) {
                if (paramsMap != null)
                    return new HTTPLoader(context, viewLoading, requestVerb, mediaType, requestUrl, paramsMap, headers, loadingMessage);
                return new HTTPLoader(context, viewLoading, requestVerb, mediaType, requestUrl, params, headers, loadingMessage);
            }

            @Override
            public void onLoadFinished(@NonNull Loader<HTTPResponse> loader, @NonNull HTTPResponse response) {
                if (response.isSuccess()) {
                    dispatchCallback(callback, response);
                    loaderManager.destroyLoader(requestLoaderId);
                    return;
                }
                dispatchCallback(callback, response);
                if (viewLoading == null) loaderManager.destroyLoader(requestLoaderId);
            }

            @Override
            public void onLoaderReset(Loader<HTTPResponse> loader) {}
        });
    }

    /**
     * Deliver the result if any callback is available. If the callback is not null also try to parse the result with the generic type.
     * @param callback a {@link OnControllerResult} to be called when the request is finish.
     * @param response a {@link HTTPResponse} containing the information about the request.
     * @param <T> the return type to use when parsing the result to delivery by {@link OnControllerResult#onResultSuccess(T)}.
     */
    private static <T> void dispatchCallback(@Nullable OnControllerResult<T> callback, @NonNull HTTPResponse response) {
        if (callback != null) {
            if (response.isSuccess()) {
                Type callbackReturnType = getCallbackType(callback);
                // Check if the generic type is String and then do a redundant cast to return the responseMessage as String
                // TODO: how wrong is this workaround?
                if (callbackReturnType != null && callbackReturnType.toString().contains("String")) {
                    callback.onResultSuccess((T) response.getResponseMessage());
                } else {
                    try {
                        // Otherwise cast the response using GSON and the generic type
                        callback.onResultSuccess(new Gson().<T>fromJson(response.getResponseMessage(), callbackReturnType));
                    } catch (JsonParseException e) {
                        // If a JSON Parse Exception occurs we call the callback passing the result as a failure and the message from the exception
                        callback.onResultFailure(new HTTPResponse(response.getStatusCode(), false, e.getMessage()));
                    }
                }
            } else {
                // The call has failed. Just use the callback to propagate the failure.
                callback.onResultFailure(response);
            }
        }
    }

    private static <T> Type getCallbackType(@NonNull OnControllerResult<T> callback) {
        Type[] genericInterfaces = callback.getClass().getGenericInterfaces();
        for (Type genericInterface : genericInterfaces) {
            if (genericInterface instanceof ParameterizedType) {
                Type[] genericTypes = ((ParameterizedType) genericInterface).getActualTypeArguments();
                return genericTypes[0];
            }
        }
        return null;
    }

    public interface OnControllerResult<T> {
        void onResultSuccess(@Nullable T result);
        void onResultFailure(@NonNull HTTPResponse response);
    }

}
