package br.com.neomode.httploader.connectivity;

import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import static android.content.Context.CONNECTIVITY_SERVICE;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 */
public class ConnectivityCheckService extends BroadcastReceiver {

    public ConnectivityCheckService() {}

    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            changeConnectivityStatus(true);
        } else {
            changeConnectivityStatus(false);
        }
    }

    /**
     * Change the status of app conectivity on {@link ConnectivityCheck} to warn listeners
     * @param hasConnectivity {@link Boolean#TRUE} if the user has any type of connection to the internet, {@link Boolean#FALSE} otherwise
     */
    private void changeConnectivityStatus(boolean hasConnectivity) {
        ConnectivityCheck.getInstance().setHasConnectivity(hasConnectivity);
    }

}
