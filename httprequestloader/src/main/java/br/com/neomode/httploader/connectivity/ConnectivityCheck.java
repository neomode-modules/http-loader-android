package br.com.neomode.httploader.connectivity;

import android.support.annotation.NonNull;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by John on 19/09/16.
 * Please add lines to this archive with all the love in the universe!
 * If you have problems with the code written right over here, please mail me: joao@neomode.com.br
 */
public class ConnectivityCheck extends Observable {

    // Singleton part
    private static ConnectivityCheck ourInstance = new ConnectivityCheck();
    public static ConnectivityCheck getInstance() {
        return ourInstance;
    }

    private ConnectivityCheck() {}

    private boolean hasConnectivity = true;

    public boolean isHasConnectivity() {
        return hasConnectivity;
    }

    public void setHasConnectivity(boolean hasConnectivity) {
        this.hasConnectivity = hasConnectivity;
        setChanged();
        notifyObservers();
    }

    public void registerObserver(@NonNull Observer observer) {
        addObserver(observer);
    }

    public void unregisterObserver(@NonNull Observer observer) {
        deleteObserver(observer);
    }

}
