# HTTPLoader

HTTP Loader is a simple project that make safe and lifecycling respectful HTTP requests.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Installing

Download the [latest JAR](http://search.maven.org/remotecontent?filepath=br/com/neomode/formvalidation/neomodeValidationForm/1.0/neomodeValidationForm-1.0-sources.jar) or grab via Maven:

```
<dependency>
  <groupId>br.com.neomode.httploader</groupId>
  <artifactId>neomodeValidationForm</artifactId>
  <version>1.0</version>
</dependency>
```

or Gradle:

```
compile 'br.com.neomode.httploader:neomodeHTTPLoader:1.0'
```

#### Setup

If you need to have a pair of values that must be sent on every HTTP call as headers (such as authorization key and value) you can call this method at any moment:

```
HTTPLoaderHelper.addDefaultHeaders(Context, HashMap<String, String);
```

### Using it

A sample using the library can be found in [here](https://gitlab.com/neomode-modules/http-loader-android/tree/master/app/src/main/java/br/com/neomode/httploadersample).

The library is actually simple to use. All requests must be called through HTTPController. The controller is responsible of everything that happens until your request is finished.

```
HTTPController.doHTTPRequest(1, MainActivity.this, getSupportLoaderManager(), mActivityBinding.httpLoadingView, url,
                            getVerb(), getParams(), getHeaders(), new HTTPController.OnControllerResult<ArrayList<State>>() {
                                @Override
                                public void onResultSuccess(@Nullable ArrayList<State> result) {
                                    Toast.makeText(MainActivity.this, "Success: " + result.size(), Toast.LENGTH_LONG).show();
                                }

                                @Override
                                public void onResultFailure(@NonNull HTTPResponse response) {
                                    Toast.makeText(MainActivity.this, "Error: " + response.getResponseMessage(), Toast.LENGTH_LONG).show();
                                }
                            });
```

If the <b>viewLoading</b> parameter is used when calling doHTTPRequest, the library will show an loading inside the view and also - if the request goes wrong - will show a retry button with a automatic retry if the internet was offline and then goes to online.

If the <b>viewLoading</b> parameter is null you need to show a loading and a error (if you want it) 

## Built With

* [okhttp](https://github.com/square/okhttp) - An HTTP & HTTP/2 client for Android and Java applications.
* [Loader](https://developer.android.com/reference/android/content/Loader.html) - A class that performs asynchronous loading of data.
* [GSON](https://github.com/google/gson) - Gson is a Java library that can be used to convert Java Objects into their JSON representation.
