package br.com.neomode.httploadersample;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

import br.com.neomode.httploader.http.HTTPController;
import br.com.neomode.httploader.http.HTTPLoader;
import br.com.neomode.httploader.http.HTTPResponse;
import br.com.neomode.httploader.http.HTTPVerb;
import br.com.neomode.httploadersample.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding mActivityBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Check if the uri is a valid http one
        mActivityBinding.floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = mActivityBinding.editUrl.getText().toString();
                if (URLUtil.isNetworkUrl(url)) {
                    HTTPController.doHTTPRequest(1, MainActivity.this, getSupportLoaderManager(), mActivityBinding.httpLoadingView, url,
                            getVerb(), HTTPLoader.JSON, getParams(), null, getHeaders(), "", new HTTPController.OnControllerResult<ArrayList<State>>() {
                                @Override
                                public void onResultSuccess(@Nullable ArrayList<State> result) {
                                    Toast.makeText(MainActivity.this, "Success: " + result.size(), Toast.LENGTH_LONG).show();
                                }

                                @Override
                                public void onResultFailure(@NonNull HTTPResponse response) {
                                    Toast.makeText(MainActivity.this, "Error: " + response.getResponseMessage(), Toast.LENGTH_LONG).show();
                                }
                            });
                }
            }
        });
    }

    private HTTPVerb getVerb() {
        switch (mActivityBinding.spinnerVerb.getSelectedItemPosition()) {
            case 0:
                return HTTPVerb.GET;
            case 1:
                return HTTPVerb.POST;
            case 2:
                return HTTPVerb.PATCH;
            case 3:
                return HTTPVerb.PUT;
            case 4:
                return HTTPVerb.DELETE;
            default:
                return HTTPVerb.GET;
        }
    }

    private HashMap<String, String> getParams() {
        HashMap<String, String> params = new HashMap<>();
        // try to split any parameters filled on the field
        String paramsText = mActivityBinding.editParams.getText().toString();
        paramsText = paramsText.replace("[", "");
        paramsText = paramsText.replace("]", "");
        String[] paramsArray = paramsText.split(",");
        if (paramsArray.length > 0) {
            for (String param: paramsArray) {
                String[] keyAndValue = param.split(":");
                if (keyAndValue.length  == 2) {
                    params.put(keyAndValue[0].trim(), keyAndValue[1].trim());
                }
            }
        }
        return params;
    }

    private HashMap<String, String> getHeaders() {
        HashMap<String, String> headers = new HashMap<>();
        // try to split any parameters filled on the field
        String paramsText = mActivityBinding.editHeaders.getText().toString();
        paramsText = paramsText.replace("[", "");
        paramsText = paramsText.replace("]", "");
        String[] paramsArray = paramsText.split(",");
        if (paramsArray.length > 0) {
            for (String param: paramsArray) {
                String[] keyAndValue = param.split(":");
                if (keyAndValue.length  == 2) {
                    headers.put(keyAndValue[0].trim(), keyAndValue[1].trim());
                }
            }
        }
        return headers;
    }

}
